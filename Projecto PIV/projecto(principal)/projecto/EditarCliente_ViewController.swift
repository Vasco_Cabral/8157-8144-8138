//
//  EditarCliente_ViewController.swift
//  projecto
//
//  Created by Vasco on 08/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class EditarCliente_ViewController: UIViewController {
    
    
    @IBOutlet weak var tfuserid: UITextField!
    @IBOutlet weak var tfpassword: UITextField!
    @IBOutlet weak var tfnome: UITextField!
    @IBOutlet weak var tftelefone: UITextField!
    @IBOutlet weak var tfmorada: UITextField!
    @IBOutlet weak var tfemail: UITextField!
    
    
    var userid = PassaDados_ViewController.dadosGlobal.dados
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let registo = Registos(context: ctx)
        
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Registos")
        
        let filtro = NSPredicate(format: "userd == %@", PassaDados_ViewController.dadosGlobal.dados)
        
        request.predicate = filtro
        
        do{
            
            let users = try ctx.fetch(request)
            
            if users.count > 0 {
                
                for user in users {
                    
                    let id = (user as AnyObject).value(forKey: "userd")
                    let pass = (user as AnyObject).value(forKey: "password")
                    let nome = (user as AnyObject).value(forKey: "nome")
                    let telef = (user as AnyObject).value(forKey: "telefone") as! Int
                    let mor = (user as AnyObject).value(forKey: "morada")
                    let mail = (user as AnyObject).value(forKey: "email")
                    
                    tfuserid.text! = id as! String
                    tfpassword.text! = pass as! String
                    tfnome.text! = nome as! String
                    tftelefone.text! = String(telef)
                    tfmorada.text! = mor as! String
                    tfemail.text! = mail as! String
  
                }
            }
        } catch{}
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    ///////////////Funçao de nao ter digitos no nome///////////////
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    ////////////////////////////////////////////////////////
    
    @IBAction func btn_Editar(_ sender: Any) {
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let registo = Registos(context: ctx)
        
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Registos")
        
        let filtro = NSPredicate(format: "userd == %@", PassaDados_ViewController.dadosGlobal.dados)
        
        request.predicate = filtro
        
        do{
            
            let users = try ctx.fetch(request)
            
            if users.count > 0 {
                
                for user in users {
                    
                    if tfnome.text == ""
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Campo nome vazio", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("nome vazio")
                    }
                    else if tfuserid.text == ""
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Campo User vazio", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("user vazio")
                        
                    }
                    else if tfpassword.text == ""
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Campo Password vazio", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("password vazia")
                        
                    }
                        
                    else if tfmorada.text == ""
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Campo Morada vazio", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("morada vazia")
                        
                    }
                        
                    else if tfemail.text == ""
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Campo Email vazio", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("email vazio")
                        
                    }
                    else if (tfpassword.text?.characters.count)! < 6
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Campo Password deve ter 6 caracteres", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("password nao tem 6 caracteres")
                        
                    }
                    else{
                        
                        do
                        {
                            (user as AnyObject).setValue(tfuserid.text!, forKey: "userd")
                            (user as AnyObject).setValue(tfpassword.text!, forKey: "password")
                            (user as AnyObject).setValue(tfpassword.text!, forKey: "password2")
                            (user as AnyObject).setValue(tfnome.text!, forKey: "nome")
                            (user as AnyObject).setValue(Int(tftelefone.text!), forKey: "telefone")
                            (user as AnyObject).setValue(tfmorada.text!, forKey: "morada")
                            (user as AnyObject).setValue(tfemail.text!, forKey: "email")
                            
                            try ctx.save()
                            print("guardado")
                            
                            // cria um alerta
                            let alert = UIAlertController(title: "Novos Dados", message: "UserID: \(tfuserid.text!)\nNome: \(tfnome.text!)\nTelefone: \(tftelefone.text!)\nMorada: \(tfmorada.text!)\nEmail: \(tfemail.text!)", preferredStyle: UIAlertControllerStyle.alert)
                            
                            // adiciona um botão ok no alerta
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                                {
                                    [unowned self] (action) -> Void in
                                    self.dismiss(animated: false, completion: nil)
                                    
                            }))
                            
                            // Mostra o alerta
                            self.present(alert, animated: true, completion: nil)
                        }
                        catch
                        {
                            print("erro")
                        }
                    }
                }
            }
        }
        catch{}
}
}

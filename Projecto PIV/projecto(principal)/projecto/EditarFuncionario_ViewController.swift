//
//  EditarFuncionario_ViewController.swift
//  projecto
//
//  Created by Vasco on 09/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class EditarFuncionario_ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tfuserid: UITextField!
    @IBOutlet weak var tfpassword: UITextField!
    @IBOutlet weak var tfnome: UITextField!
    @IBOutlet weak var tfemail: UITextField!

    var userid = PassaDados_ViewController.dadosGlobal.dados

    override func viewDidLoad() {
        super.viewDidLoad()

        ///////chamei aqui///////
        self.tfnome.delegate = self;
        ///////////////////////////
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Funcionarios")
        
        let filtro = NSPredicate(format: "userid == %@", PassaDados_ViewController.dadosGlobal.dados)
        
        request.predicate = filtro
        
        do{
            
            let users = try ctx.fetch(request)
            
            if users.count > 0 {
                
                for user in users {
                    
                    let id = (user as AnyObject).value(forKey: "userid")
                    let pass = (user as AnyObject).value(forKey: "password")
                    let nome = (user as AnyObject).value(forKey: "nome")
                    let mail = (user as AnyObject).value(forKey: "email")
                    
                    tfuserid.text! = id as! String
                    tfpassword.text! = pass as! String
                    tfnome.text! = nome as! String
                    tfemail.text! = mail as! String
                    
                }
            }
        }
        catch
        {
            
        }

    }
    
    ///////////////Funçao de nao ter digitos no nome///////////////
    @objc(textField:shouldChangeCharactersInRange:replacementString:) func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    ////////////////////////////////////////////////////////

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btn_Editar(_ sender: Any) {
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Funcionarios")
        
        let filtro = NSPredicate(format: "userid == %@", PassaDados_ViewController.dadosGlobal.dados)
        
        request.predicate = filtro
        
        
        
        //////////////////////NOVO IF///////////////////////
        
        let tfname = tfnome.text
        let tfmail = tfemail.text
        let tfpass = tfpassword.text
        let tfuser = tfuserid.text
        
        if tfuser == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo User vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else if tfname == ""
        {
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Nome vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
        }
        else if tfmail == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Email vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
        }
            
            
        else if (tfpass?.characters.count)! < 6
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Password deve ter 6 caracteres", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
        else{
            
            do{
                let users = try ctx.fetch(request)
                
                
                if users.count > 0 {
                    
                    for user in users {
                        
                        (user as AnyObject).setValue(tfuserid.text!, forKey: "userid")
                        (user as AnyObject).setValue(tfpassword.text!, forKey: "password")
                        (user as AnyObject).setValue(tfnome.text!, forKey: "nome")
                        (user as AnyObject).setValue(tfemail.text!, forKey: "email")
                        
                        do
                        {
                            try ctx.save()
                            print("Guardado")
                            
                            // cria um alerta
                            let alert = UIAlertController(title: "Editado", message: "UserID: \(tfuser!)\nNome: \(tfname!)\nEmail: \(tfmail!)", preferredStyle: UIAlertControllerStyle.alert)
                            
                            // adiciona um botão ok no alerta
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                                {
                                    [unowned self] (action) -> Void in
                                    
                                    self.dismiss(animated: false, completion: nil)
                            }))
                            
                            // Mostra o alerta
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        catch
                        {
                            print("Erro! Os dados nao foram inseridos")
                        }
                        
                    }
                }
            }
            catch
            {
                
            }
        }
        
    }
}

//
//  HistoricoFuncionario_TableViewController.swift
//  projecto
//
//  Created by Vasco on 09/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class HistoricoFuncionario_TableViewController: UITableViewController {

    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var serv: [Servicos] = []
    var selecIndex:Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func fetchData() {
        
        do
        {
            serv = try context.fetch(Servicos.fetchRequest())
            print(serv)
            DispatchQueue.main.async
            {
                self.tableView.reloadData()
            }
        }
        catch
        {
            print("Erro. Nao foi possivel executar o fetch")
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
    }
    
    
 
    @IBAction func btn_Back(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let servs = self.serv[indexPath.row]
        
        let estado = (servs as AnyObject).value(forKey: "estado") as? String
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YY"
        let currentDate = formatter.string(from: date)
        
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        let currentTime = timeFormatter.string(from: date)
        
        if estado == "Serviço Terminado" || estado == "Serviço Cancelado"
        {

        }
        
        else if estado == "Serviço Iniciado"
        {
            let term = UITableViewRowAction(style: .default, title: "Terminar") { (action, indexPath) in
                
                (servs as AnyObject).setValue("Serviço Terminado", forKey: "estado")
                (servs as AnyObject).setValue(currentDate, forKey: "date")
                (servs as AnyObject).setValue(currentTime, forKey: "time")
                
                do
                {
                    try self.context.save()
                    tableView.reloadData()
                    print("Guardado")
                }
                catch
                {
                    print("Nao guardou")
                }
                print(servs)
                
            }

            return[term]
        }
        else if estado == "Serviço Aceite"
        {
            let inic = UITableViewRowAction(style: .default, title: "Iniciar") { (action, indexPath) in
                
                (servs as AnyObject).setValue("Serviço Iniciado", forKey: "estado")
                (servs as AnyObject).setValue(currentDate, forKey: "date")
                (servs as AnyObject).setValue(currentTime, forKey: "time")
                
                do
                {
                    try self.context.save()
                    tableView.reloadData()
                    print("Guardado")
                }
                catch
                {
                    print("Nao guardou")
                }
                print(servs)
            }
            
            let cancelar = UITableViewRowAction(style: .default, title: "Cancelar") { (action, indexPath) in
                
                (servs as AnyObject).setValue("Serviço Cancelado", forKey: "estado")
                (servs as AnyObject).setValue(currentDate, forKey: "date")
                (servs as AnyObject).setValue(currentTime, forKey: "time")
                
                do
                {
                    try self.context.save()
                    tableView.reloadData()
                    print("Guardado")
                }
                catch
                {
                    print("Nao guardou")
                }
                print(servs)
            }
            
            return[inic, cancelar]
        }
        else
        {
            let aceit = UITableViewRowAction(style: .default, title: "Aceitar") { (action, indexPath) in
                
                (servs as AnyObject).setValue("Serviço Aceite", forKey: "estado")
                (servs as AnyObject).setValue(currentDate, forKey: "date")
                (servs as AnyObject).setValue(currentTime, forKey: "time")
                
                do
                {
                    try self.context.save()
                    tableView.reloadData()
                    print("Guardado")
                }
                catch
                {
                    print("Nao guardou")
                }
                print(servs)
            }
            
            return[aceit]
        }
       // aceit.backgroundColor = UIColor(red: 0/255, green: 177/255, blue: 106/255, alpha: 1.0)
        //inic.backgroundColor = UIColor(red: 0/255, green: 150/255, blue: 106/255, alpha: 1.0)
        //term.backgroundColor = UIColor(red: 0/255, green: 130/255, blue: 106/255, alpha: 1.0)
        
    
        return[]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = serv[indexPath.row].trabalho
        var detalhe = serv[indexPath.row].estado
        
        let date = serv[indexPath.row].date
        let time = serv[indexPath.row].time
        
        if let date = date, let time = time {
            let timeStamp = " \(date) \(time)"
            cell.detailTextLabel?.text = detalhe! + timeStamp
            
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return serv.count
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selecIndex = indexPath.row
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        performSegue(withIdentifier: "segue_DadosServ", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "segue_DadosServ" {
            
            let dadosServ = segue.destination as! DadosServicos_ViewController
            dadosServ.servs = serv[selecIndex!]
        }
        
    }
    
}


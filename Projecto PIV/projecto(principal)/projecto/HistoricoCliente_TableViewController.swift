//
//  HistoricoCliente_TableViewController.swift
//  projecto
//
//  Created by Vasco on 04/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class HistoricoCliente_TableViewController: UITableViewController {
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var serv: [Servicos] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func fetchData() {
        
        do {
            
            let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Servicos")
            
            let filtro = NSPredicate(format: "userid = %@", PassaDados_ViewController.dadosGlobal.dados)
            
            request.predicate = filtro
            
            serv = try context.fetch(request) as! [Servicos]
            
            print(serv)
        
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print("Couldn't Fetch Data")
        }
        
        
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
    }
    
    
    @IBAction func btn_Back(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = serv[indexPath.row].trabalho
        var detalhe = serv[indexPath.row].estado
        
        let date = serv[indexPath.row].date
        let time = serv[indexPath.row].time
        
        if let date = date, let time = time {
            let timeStamp = " \(date) \(time)"
            cell.detailTextLabel?.text = detalhe! + timeStamp
            
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return serv.count
        
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        let cancelar = UITableViewRowAction(style: .default, title: "Cancelar Serviço") { (action, indexPath) in
            // delete item at indexPath
            
            let servs = self.serv[indexPath.row]
            
            
            let estado = (servs as AnyObject).value(forKey: "estado") as? String
            
            if estado == "Serviço Iniciado"
            {
                // cria um alerta
                let alert = UIAlertController(title: "Erro!", message: "O serviço já foi iniciado. Só pode cancelar o serviço antes do mesmo ser iniciado.", preferredStyle: UIAlertControllerStyle.alert)
                
                // adiciona um botão ok no alerta
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                // Mostra o alerta
                self.present(alert, animated: true, completion: nil)
            }
            else if estado == "Serviço Concluido"
            {
                // cria um alerta
                let alert = UIAlertController(title: "Erro!", message: "O serviço já está concluído. O serviço nao pode ser cancelado após a sua conclusao", preferredStyle: UIAlertControllerStyle.alert)
                
                // adiciona um botão ok no alerta
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                // Mostra o alerta
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                (servs as AnyObject).setValue("Serviço Cancelado", forKey: "estado")
                
                do
                {
                    try self.context.save()
                    tableView.reloadData()
                    print("Guardado")
                }
                catch
                {
                    print("Nao guardou")
                }
                print(servs)
            }
        }
        
        cancelar.backgroundColor = UIColor(red: 0/255, green: 177/255, blue: 106/255, alpha: 1.0)
        
        return [cancelar]
        
    }

    
}

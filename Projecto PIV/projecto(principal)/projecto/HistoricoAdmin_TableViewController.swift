//
//  HistoricoAdmin_TableViewController.swift
//  projecto
//
//  Created by Vasco on 05/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit

class HistoricoAdmin_TableViewController: UITableViewController {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var serv: [Servicos] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func fetchData() {
        
        do
        {
            serv = try context.fetch(Servicos.fetchRequest())
            print(serv)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print("Nao foi feito o fetch")
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
    }
    
    
    @IBAction func btn_back(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = serv[indexPath.row].trabalho
        var detalhe = serv[indexPath.row].estado
        
        let date = serv[indexPath.row].date
        let time = serv[indexPath.row].time
        
        if let date = date, let time = time
        {
            let timeStamp = " \(date) \(time)"
            cell.detailTextLabel?.text = detalhe! + timeStamp
            
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return serv.count
        
    }
    
    
}

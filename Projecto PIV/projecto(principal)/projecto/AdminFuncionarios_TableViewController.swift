//
//  AdminFuncionarios_TableViewController.swift
//  projecto
//
//  Created by Vasco on 06/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit

class AdminFuncionarios_TableViewController: UITableViewController {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var funcio: [Funcionarios] = []
    var selecIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func fetchData() {
        
        do
        {
            funcio = try context.fetch(Funcionarios.fetchRequest())
            print(funcio)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print("Nao fez o fetch")
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
    }
    
    
 
    @IBAction func btn_Back(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = funcio[indexPath.row].userid
        cell.detailTextLabel?.text = funcio[indexPath.row].nome
        
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return funcio.count
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(funcio[indexPath.row].userid)
        
        selecIndex = indexPath.row
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: "segue_DadosFunc", sender: self)
        
    }
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let apagar = UITableViewRowAction(style: .default, title: "Apagar") { (action, indexPath) in
            // delete item at indexPath
            
            let funcios = self.funcio[indexPath.row]
            self.context.delete(funcios)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            self.funcio.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
        
        let editar = UITableViewRowAction(style: .default, title: "Editar") { (action, indexPath) in
            // delete item at indexPath
            
            self.selecIndex = indexPath.row
            
            tableView.deselectRow(at: indexPath, animated: true)
            
            self.performSegue(withIdentifier: "segue_EditFunc", sender: self)
            
        }
        
        apagar.backgroundColor = UIColor(red: 0/255, green: 177/255, blue: 106/255, alpha: 1.0)
        editar.backgroundColor = UIColor(red: 0/255, green: 150/255, blue: 106/255, alpha: 1.0)
        
        return [apagar, editar]
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_EditFunc" {
            // Set item here
            let editFunc = segue.destination as! EditFunc_ViewController
            editFunc.funcios = funcio[selecIndex!]
        }
            
        else if segue.identifier == "segue_DadosFunc" {
            // Set item here
            let dadosFunc = segue.destination as! DadosFunc_ViewController
            dadosFunc.funcios = funcio[selecIndex!]
        }
        
    }
    
}

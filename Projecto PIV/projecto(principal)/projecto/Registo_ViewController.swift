//
//  Registo_ViewController.swift
//  projecto
//
//  Created by Vasco on 27/05/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class Registo_ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var tfuserid: UITextField!
    @IBOutlet weak var tfpassword: UITextField!
    @IBOutlet weak var tfpassword2: UITextField!
    @IBOutlet weak var tfnome: UITextField!
    @IBOutlet weak var tftelefone: UITextField!
    @IBOutlet weak var tfmorada: UITextField!
    @IBOutlet weak var tfemail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ///////chamei aqui///////
        self.tfnome.delegate = self;
        ///////////////////////////

    }

    ///////////////Funçao de nao ter digitos no nome///////////////
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    ////////////////////////////////////////////////////////



       ////////////////////////////////////////////////////////
    
    
    @IBAction func btnBack(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
    
    
    @IBAction func btnConcluir(_ sender: Any) {
        
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let registo = Registos(context: ctx)
 

        let tfname = tfnome.text
        let tfmail = tfemail.text
        let tfmor = tfmorada.text
        let tftelef = Int(tftelefone.text!)
        let tfpass = tfpassword.text
        let tfpass2 = tfpassword2.text
        let tfuser = tfuserid.text
        
        //////////////////////NOVO IF///////////////////////
        
        if tfname == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo nome vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            print("nome vazio")
        }
            
        else if tfuser == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo User vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            print("user vazio")
            
        }
        else if tfmor == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Morada vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            print("morada vazia")
            
        }
        else if tfmail == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Email vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            print("email vazio")
            
        }
        else if (tfpass?.characters.count)! < 6
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Password deve ter 6 caracteres", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            print("password n tem 6 caracteres")
            
        }
        else if tfpass != tfpass2
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campos Password são diferentes", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            print("passwords diferentes")
            
        }
        else{
            
            do{
                registo.setValue(tfuser, forKey: "userd")
                registo.setValue(tftelef, forKey: "telefone")
                registo.setValue(tfpass, forKey: "password")
                registo.setValue(tfpass2, forKey: "password2")
                registo.setValue(tfname, forKey: "nome")
                registo.setValue(tfmor, forKey: "morada")
                registo.setValue(tfmail, forKey: "email")
                
                try ctx.save()
                print("Guardado")
                
                // cria um alerta
                let alert = UIAlertController(title: "Registado", message: "UserID: \(tfuser!)\n Email: \(tfmail!)", preferredStyle: UIAlertControllerStyle.alert)
                
                // adiciona um botão ok no alerta
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                    {
                        [unowned self] (action) -> Void in
                        
                        self.performSegue(withIdentifier: "segue_login", sender: self)
                }))
                
                // Mostra o alerta
                self.present(alert, animated: true, completion: nil)
                
                
            }
            catch
            {
                print("Erro! Os dados nao foram inseridos")
            }
            
            
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

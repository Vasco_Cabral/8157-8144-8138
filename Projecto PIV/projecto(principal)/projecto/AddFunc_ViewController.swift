//
//  AddFunc_ViewController.swift
//  projecto
//
//  Created by Vasco on 05/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class AddFunc_ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var tfuserid: UITextField!
    @IBOutlet weak var tfpassword: UITextField!
    @IBOutlet weak var tfnome: UITextField!
    @IBOutlet weak var tfemail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tfnome.delegate = self;
        
        // Do any additional setup after loading the view.
    }
    
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    @IBAction func btn_Back(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
  
    @IBAction func btn_adicionar(_ sender: Any) {
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let addfunc = Funcionarios(context: ctx)
        
        let tfname = tfnome.text
        let tfmail = tfemail.text
        let tfuser = tfuserid.text
        let tfpass = tfpassword.text
        
        
        //////////////Inserir se não for vazio////////////////////
        
        if tfname == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Nome vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else if tfuser == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo User vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
            
            
        else if tfmail == ""
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Email vazio", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
        else if (tfpass?.characters.count)! < 6
        {
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro!", message: "Campo Password deve ter 6 caracteres", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
        }
            
        else{
            
            do{
                addfunc.setValue(tfuser, forKey: "userid")
                addfunc.setValue(tfpass, forKey: "password")
                addfunc.setValue(tfname, forKey: "nome")
                addfunc.setValue(tfmail, forKey: "email")
                
                do
                {
                    try ctx.save()
                    print("Guardado")
                    
                    // cria um alerta
                    let alert = UIAlertController(title: "Registado", message: "UserID: \(tfuser!)\nNome: \(tfname!)\nEmail: \(tfmail!)", preferredStyle: UIAlertControllerStyle.alert)
                    
                    // adiciona um botão ok no alerta
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                        {
                            [unowned self] (action) -> Void in
                            
                            self.dismiss(animated: true, completion: nil)
                    }))
                    
                    // Mostra o alerta
                    self.present(alert, animated: true, completion: nil)
            
                }
                catch
                {
                    print("Erro! Os dados nao foram inseridos")
                }
                
                
            }
        }
    }
}

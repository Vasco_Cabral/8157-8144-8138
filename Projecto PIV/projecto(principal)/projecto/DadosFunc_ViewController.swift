//
//  DadosFunc_ViewController.swift
//  projecto
//
//  Created by Vasco on 07/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit

class DadosFunc_ViewController: UIViewController {
    
    @IBOutlet weak var lbluser: UILabel!
    @IBOutlet weak var lblnome: UILabel!
    @IBOutlet weak var lblemail: UILabel!
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var funcios: Funcionarios!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureEntryData(entry: funcios)
        
        print(funcios)
        
        // Do any additional setup after loading the view.
    }
    
    func configureEntryData(entry: Funcionarios) {
        lbluser!.text = entry.userid
        lblnome!.text = entry.nome
        lblemail!.text = entry.email
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

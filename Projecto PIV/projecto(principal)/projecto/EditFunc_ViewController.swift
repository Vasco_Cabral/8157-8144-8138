//
//  EditFunc_ViewController.swift
//  projecto
//
//  Created by Vasco on 06/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit

class EditFunc_ViewController: UIViewController {
    
    @IBOutlet weak var tfuserid: UITextField!
    @IBOutlet weak var tfpassword: UITextField!
    @IBOutlet weak var tfnome: UITextField!
    @IBOutlet weak var tfemail: UITextField!
    
    var funcios: Funcionarios!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureEntryData(entry: funcios)
        
        print(funcios)
        
        // Do any additional setup after loading the view.
    }

    func configureEntryData(entry: Funcionarios) {
        tfuserid!.text = entry.userid ?? "Nao há nada para editar"
        tfpassword!.text = entry.password ?? "Nao há nada para editar"
        tfnome!.text = entry.nome ?? "Nao há nada para editar"
        tfemail!.text = entry.email ?? "Nao há nada para editar"
    }
    
    @IBAction func btn_Back(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func btn_editar(_ sender: Any) {
        
        guard let novouserid = tfuserid.text, let novopass = tfpassword.text,let novonome = tfnome.text, let novoemail = tfemail.text else  {
            return
        }
        
        funcios.userid = novouserid
        funcios.password = novopass
        funcios.nome = novonome
        funcios.email = novoemail
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        dismiss(animated: true, completion: nil)
        
    }

}

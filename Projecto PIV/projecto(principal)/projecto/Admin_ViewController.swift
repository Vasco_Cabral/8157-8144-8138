//
//  Admin_ViewController.swift
//  projecto
//
//  Created by Vasco on 02/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit

class Admin_ViewController: UIViewController {

    
    @IBOutlet weak var bt: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bt.target = revealViewController();
        
        self.bt.action = #selector(SWRevealViewController.revealToggle(_:))
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }


    @IBAction func btn_Back(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  DadosServicos_ViewController.swift
//  projecto
//
//  Created by Vasco on 12/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class DadosServicos_ViewController: UIViewController {

    @IBOutlet weak var lbluser: UILabel!
    @IBOutlet weak var lblnome: UILabel!
    @IBOutlet weak var lbltelefone: UILabel!
    @IBOutlet weak var lblmorada: UILabel!
    @IBOutlet weak var lblemail: UILabel!
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var servs: Servicos!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(servs)
        
        buscadados()
        
        configureEntryData(entry: servs)
        
    }

    
    func configureEntryData(entry: Servicos) {
        lbluser?.text = entry.userid
    }
    
    
    func buscadados()
    {
        let userid = servs.userid!
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Registos")
        
        let filtro = NSPredicate(format: "userd == %@", userid)
        
        request.predicate = filtro
        
        do
        {
            
            let users = try ctx.fetch(request)
            
            if users.count > 0 {
                
                for user in users {
                    
                    let nome = (user as AnyObject).value(forKey: "nome")
                    let telef = (user as AnyObject).value(forKey: "telefone")
                    let email = (user as AnyObject).value(forKey: "email")
                    let morada = (user as AnyObject).value(forKey: "morada")
                    
                    lblnome.text! = nome as! String
                    lbltelefone.text! = String(describing: telef!)
                    lblemail.text! = email as! String
                    lblmorada.text! = morada as! String
                    
                }
            }
        } catch{}

    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

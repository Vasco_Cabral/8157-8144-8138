//
//  PedirServiço_ViewController.swift
//  projecto
//
//  Created by Vasco on 03/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit

class PedirServic_o_ViewController: UIViewController {

    @IBOutlet weak var tfservico: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func btn_back(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
    }
    
    
    
    @IBAction func btn_Servico(_ sender: Any) {
        
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let servico = Servicos(context: ctx)
        
        let tfserv = tfservico.text!
        
        servico.setValue(PassaDados_ViewController.dadosGlobal.dados, forKey: "userid")
        servico.setValue(tfserv, forKey: "trabalho")
        servico.setValue("Serviço Requesitado", forKey: "estado")
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YY"
        let currentDate = formatter.string(from: date)
        
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        let currentTime = timeFormatter.string(from: date)
        
        servico.setValue(currentDate, forKey: "date")
        servico.setValue(currentTime, forKey: "time")
        
        let dat = servico.value(forKey: "date")
        let tim = servico.value(forKey: "time")
        
        print(dat)
        print(tim)
        

        if tfserv != ""
        {
            guardar()
        }
        else
        {
            let alert = UIAlertController(title: "Erro!", message: "Nao pode requesitar serviços em branco", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func guardar()
    {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        do
        {
            try ctx.save()
            print("Guardado")
            
            // cria um alerta
            let alert = UIAlertController(title: "Sucesso", message: "O serviço foi registado com sucesso", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                {
                    [unowned self] (action) -> Void in
                    
                    self.dismiss(animated: false, completion: nil)
            }))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
            
        }
        catch
        {
            print("Erro! Os dados nao foram inseridos")
            
            // cria um alerta
            let alert = UIAlertController(title: "Erro", message: "O serviço nao foi registado com sucesso", preferredStyle: UIAlertControllerStyle.alert)
            
            // adiciona um botão ok no alerta
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                {
                    [unowned self] (action) -> Void in
                    
                    self.dismiss(animated: false, completion: nil)
            }))
            
            // Mostra o alerta
            self.present(alert, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

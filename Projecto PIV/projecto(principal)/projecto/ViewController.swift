//
//  ViewController.swift
//  projecto
//
//  Created by Vasco on 27/05/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

  ///////////////////////////////////////INICIO INSERIR ADMIN///////////////////////////////////////////
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        //////////////////FIM INSERIR ADMIN////////////////////////
        
        do{
            
            let admin = Administrador(context: ctx)
            let requestadmin = NSFetchRequest<NSFetchRequestResult> (entityName: "Administrador")
            let usersadmin = try ctx.fetch(requestadmin)
            
            
            ////////////////////////////////////////////IF DO ADMIN/////////////////////////////////////////////////////////////
            
            if usersadmin.count <= 1 {
                
                
                for adminis in usersadmin {
                    (adminis as AnyObject).setValue("root", forKey: "username")
                    (adminis as AnyObject).setValue("1234", forKey: "password")
                    let id = (adminis as AnyObject).value(forKey: "username") as! String
                    let pw = (adminis as AnyObject).value(forKey: "password") as! String
                    
                    
                    
                    print("Valor do userid na tabela: \(id)")
                    print("Valor da pass na tabela: \(pw)")
                    print("admin adicionado")
                    
                    let rq = NSFetchRequest<NSFetchRequestResult>(
                        entityName: "Administrador")
                    do{
                        let resultaddo = try ctx.fetch(rq)
                        print("resultado: \(resultaddo.count)")
                        for a in resultaddo as! [NSManagedObject]{
                            let nome = (a as AnyObject).value(forKey: "username") as! String
                            let idade = (a as AnyObject).value(forKey: "password") as! String
                            print("user: \(nome), pass: \(idade)")
                            
                        }}
                    catch{
                    }
                }
            }
            else{
                print("admin nao adicionado")
            }
            
            //////////////////FIM DO IF sobre criar o admin////////////////////////
            
        }
        catch
        {
            print("")
        }

       //////////////////FIM INSERIR ADMIN////////////////////////
    }
    

    @IBAction func btnLogin(_ sender: Any) {

        
        performSegue(withIdentifier: "segue_login", sender: " ")
        
    }
    
    
    @IBAction func btnRegisto(_ sender: Any) {
        
        performSegue(withIdentifier: "segue_registo", sender: " ")
        
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let loginView = segue.destination as? Login_ViewController
        
        let registoView = segue.destination as? Registo_ViewController
        
    }


    ////////////////////////////////////////////funçao delete/////////////////////////////////////////////////////////////
    
    func deleteAllRecords() {
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let ctx = appDelegate.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Administrador")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try ctx.execute(deleteRequest)
            try ctx.save()
            print("sucesso ao fazer delete")
        } catch {
            print ("There was an error")
        }
    }
    //////////////////////////////////////////// FIM funçao delete/////////////////////////////////////////////////////////////
    
    @IBAction func btn_DeleteAdmins(_ sender: Any) {
        
        //INSERIR ESTE BOTAO NO STORYBOARD SE FOR PRECISO APAGAR DADOS DA TABELA
        
        
        deleteAllRecords()
        
    }
    
    
}


//
//  Login_ViewController.swift
//  projecto
//
//  Created by Vasco on 27/05/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class Login_ViewController: UIViewController {

    @IBOutlet weak var tfuserid: UITextField!
    @IBOutlet weak var tfpassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
    
    
    @IBAction func btnLogin(_ sender: Any) {
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        ////////////////////////////////////////
        
        let username = tfuserid.text!
        
        let pass = tfpassword.text!
        
        let userfuncionario = tfuserid.text!
        
        let passfuncionario = tfpassword.text!
        
        let useradmin = tfuserid.text!
        
        let passadmin = tfpassword.text!
        
        PassaDados_ViewController.dadosGlobal.dados = username
        
        ///////////////////////////////////////
        
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Registos")
        
        let requestfuncionario = NSFetchRequest<NSFetchRequestResult> (entityName: "Funcionarios")
        
        let requestadmin = NSFetchRequest<NSFetchRequestResult> (entityName: "Administrador")
        
        let filtro = NSPredicate(format: "userd = %@", username)
        
        let filtrofuncionario = NSPredicate(format: "userid = %@", username)
        
        let filtroadmin = NSPredicate(format: "username = %@", username)
        
        request.predicate = filtro
        
        requestfuncionario.predicate = filtrofuncionario
        
        requestadmin.predicate = filtroadmin
        
        do{
            
            let users = try ctx.fetch(request)
            
            let usersfuncionario = try ctx.fetch(requestfuncionario)
            
            let usersadmin = try ctx.fetch(requestadmin)
            
            if users.count > 0 {
                
                for user in users {
                    
                    let id = (user as AnyObject).value(forKey: "userd") as! String
                    let pw = (user as AnyObject).value(forKey: "password") as! String
                    let name = (user as AnyObject).value(forKey: "nome") as! String
                    
                    print("Valor do userid na tabela: \(id)")
                    print("Valor da pass na tabela: \(pw)")
                    
                    
                    if id == username && pw == pass
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Sucesso", message: "Bem-Vindo \(name)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                            {
                                [unowned self] (action) -> Void in
                                
                                self.performSegue(withIdentifier: "segue_cliente", sender: self)
                        }))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("Sucesso ao Entrar")
                        
                    }
                    
                    else
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Introduza as Credendicais correctamente.", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("Cliente - Insira as credenciais correctas")
                    }
                    
                    
                }//Acaba o FOR
            }// Acaba IF
            
            else
            {
                
                print ("Cliente - Count é menor que zero")
            }
            
            //IF DO FUNCIONARIO
            
            if usersfuncionario.count > 0 {
                
                for user in usersfuncionario {
                    
                    let idfunc = (user as AnyObject).value(forKey: "userid") as! String
                    let pwfunc = (user as AnyObject).value(forKey: "password") as! String
                    let namefunc = (user as AnyObject).value(forKey: "nome") as! String
                    
                    print("Valor do userid na tabela: \(idfunc)")
                    print("Valor da pass na tabela: \(pwfunc)")
                    
                    
                    if idfunc == userfuncionario && pwfunc == passfuncionario
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Sucesso", message: "Bem-Vindo \(namefunc)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                            {
                                [unowned self] (action) -> Void in
                                
                                self.performSegue(withIdentifier: "segue_funcionario", sender: self)
                        }))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("Sucesso ao Entrar")
                        
                    }
                        
                    else
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Introduza as Credendicais correctamente.", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("Funcionario - Insira as credenciais correctas")
                    }
                    
                    
                }//Acaba o FOR
            }// Acaba IF
            else
            {
                print("Erro Funcionario - Count é zero")
            }
            
            
            // IF DO ADMIN
            
            if usersadmin.count > 0 {
                
                for user in usersadmin {
                    
                    let idadmin = (user as AnyObject).value(forKey: "username") as! String
                    let pwadmin = (user as AnyObject).value(forKey: "password") as! String
                    
                    //let name = (user as AnyObject).value(forKey: "nome") as! String
                    
                    print("Valor do userid na tabela: \(idadmin)")
                    print("Valor da pass na tabela: \(pwadmin)")
                    
                    if idadmin == useradmin && pwadmin == passadmin
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Sucesso", message: "Bem-Vindo Admin", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                            {
                                [unowned self] (action) -> Void in
                                
                                self.performSegue(withIdentifier: "segue_admin", sender: self)
                        }))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("Sucesso ao Entrar")
                        
                    }
                        
                        
                    else
                    {
                        
                        // cria um alerta
                        let alert = UIAlertController(title: "Erro!", message: "Introduza as Credendicais correctamente.", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // adiciona um botão ok no alerta
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        
                        // Mostra o alerta
                        self.present(alert, animated: true, completion: nil)
                        
                        print("Admin - Insira as credenciais correctas")
                    }
                    
                    
                }//Acaba o FOR
            }// Acaba IF
                
            else
            {
                
                print ("ADMIN - Count é menor que zero")
            }
            
        }
        catch
        {
            print("Erro ao tentar entrar")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

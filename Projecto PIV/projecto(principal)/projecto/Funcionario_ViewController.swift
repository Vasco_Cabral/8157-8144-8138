//
//  Funcionario_ViewController.swift
//  projecto
//
//  Created by Vasco on 09/06/2017.
//  Copyright © 2017 Vasco. All rights reserved.
//

import UIKit
import CoreData

class Funcionario_ViewController: UIViewController {
    
    @IBOutlet weak var bt: UIBarButtonItem!
    
    @IBOutlet weak var nomefuncionario_lbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.bt.target = revealViewController();
        
        self.bt.action = #selector(SWRevealViewController.revealToggle(_:))
        
        maisdados()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btn_Logout(_ sender: Any) {
        
        dismiss(animated: false, completion: nil)
        
    }
    
    
    @IBAction func btn_Editar(_ sender: Any) {
        
        performSegue(withIdentifier: "segue_EditFuncionario", sender: self)
        
    }
    
    
    func maisdados()
    {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        let ctx = appDelegate.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Funcionarios")
        
        let filtro = NSPredicate(format: "userid == %@", PassaDados_ViewController.dadosGlobal.dados)
        
        request.predicate = filtro
        
        do{
            
            let users = try ctx.fetch(request)
            
            if users.count > 0 {
                
                for user in users {
                    
                    let id = (user as AnyObject).value(forKey: "userid") as! String
                    
                    let name = (user as AnyObject).value(forKey: "nome") as! String
                    
                    print("Valor do userid na tabela: \(id)")
                    
                    if id == PassaDados_ViewController.dadosGlobal.dados
                    {
                        nomefuncionario_lbl.text = "Olá " + name
                    }
                }
            }
        }
        catch{}
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_EditFuncionario" {
            // Set item here
            segue.destination as! EditarFuncionario_ViewController
        }
    }
    

}
